""" This module parses server logs in *Common Log File* format and outputs a
summary in a csv file.
"""
import csv
import fileinput
import re

CLF_Pattern = re.compile(
        r"([\d\.]+)\s+"     # digits and .'s: host
        r"(\S+)\s+"         # non-space: logname
        r"(\S+)\s+"         # non-space: user
        r"\[(.+?)\]\s+"     # Everything in []: time
        r'"(.+?)"\s+'       # Everything in "": request
        r"(\d+)\s+"         # digits: status
        r"(\S+)\s+"         # non-space: bytes
        r'"(.*?)"\s+'       # Everything in "": referrer
        r'"(.*?)"\s*'       # Everything in "": user agent
)

output_file_headers = ['Host', 'Log Name', 'User', 'Time', 'Request',
        'Response Status', 'Served Bytes', 'Referer', 'User Agent'
]

output_file_path    = '/tmp/mapme-log.csv'

with open(output_file_path, "w") as target:
    wtr = csv.DictWriter(target, fieldnames=output_file_headers)
    wtr.writeheader()
    with fileinput.input(files='/var/log/nginx/access.log') as source:
        line_iter   = (line for line in source)
        match_iter  = (CLF_Pattern.match(line) for line in line_iter)
        for m in match_iter:
            if m is not None:
                wtr.writerow({
                    'Host'				: m.groups()[0],
                    'Log Name'			: m.groups()[1],
                    'User'				: m.groups()[2],
                    'Time'				: m.groups()[3],
                    'Request'			: m.groups()[4],
                    'Response Status'	: m.groups()[5],
                    'Served Bytes'		: m.groups()[6],
                    'Referer'			: m.groups()[7],
                    'User Agent'		: m.groups()[8]
                })
