""" Illustration of a function decorator that takes an argument.
"""

import click
import functools
import logging
import sys

# Define decorator
def debug_named(log_name):
	def concrete_decorator(fn):
		@functools.wraps(fn)
		def wrapped(*args, **kw):
			log = logging.getLogger(log_name)
			log.debug("%s(%r, %r)", fn.__name__, args, kw,)
			result = fn(*args, **kw)
			log.debug("%s = %r", fn.__name__, result)
			return result
		return wrapped
	return concrete_decorator

# Apply decorator
@debug_named("recursion")
def ackerman(m, n):
	if m == 0:
		return n + 1
	elif m > 0 and n == 0:
		return ackerman(m - 1, 1)
	elif m > 0 and n > 0:
		return ackerman(m - 1, ackerman(m, n - 1))

# Configure logging
# Use root logger
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
click.secho(str(ackerman(2,4)), fg='green')
