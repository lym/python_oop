import datetime

from post import Post
from blog import Blog
from settings import TIME_ZONE

travel = Blog("Travel")
travel.append(
    Post(
        date        = datetime.datetime(2016, 1, 13, 10, 18, tzinfo=TIME_ZONE),
        title       ='Hard Ground',
        rst_text    = """Some embarrassing revelation. Including ☹ and ⎕""",
        tags        = ("#RedRanger", "#whitby42", "#ICW")
    )
)

travel.append(
    Post(
        date        = datetime.datetime(2016, 1, 12, 8, 20, tzinfo=TIME_ZONE),
        title       = "Anchor Follies",
        rst_text    = """Some witty epigram. Including < & > characters.""",
        tags        = ("#RedRanger", "#Whitby42", "#Mistakes"),
    )
)

# Rendering
from jinja2 import Template

f = open('templates/index.html') #.read()
blog_template = Template(f.read())
f.close()
print(blog_template.render(tags=travel.by_tag(), **travel.as_dict()))
# TODO: Use the docutils rst2html tool to generate an HTML file that can can be
# rendered by a web browser


# JSON Output/Encoding
# Using default encoding algorithm
# Outputs to '/tmp/blog.json' on the file system
import json
with open('/tmp/blog.json', 'w', encoding='UTF-8') as json_output:
    json_output.write(json.dumps(travel.as_dict(), indent=4))


# Using a custom encoding algorithm
# Outputs to '/tmp/blog.json' on the file system
def blog_encode(object):
    if isinstance(object, datetime.datetime):
        return dict(
            __class__ = "datetime.datetime",
            __args__ = [],
            __kw__ = dict(
                year = object.year,
                month = object.month,
                day = object.day,
                hour = object.hour,
                minute = object.minute,
                second = object.second
            )
        )
    elif isinstance(object, Post):
        return dict(
            __class__   = "Post",
            __args      = [],
            __kw__      = dict(
                __class__       = "Post",
                __args__        = [],
                __kw__          = dict(
                    date    = object.date,
                    title   = object.title,
                    rst_text = object.rst_text,
                    tags    = object.tags
                )
            )
        )
    elif isinstance(object, Blog):
        return dict(
            __class__ = "Blog",
            __args__    = [
                object.title,
                object.entries
            ],
            __kw__ = {}
        )
    else:
        return json.JSONEncoder.default(o)

with open('/tmp/blogv2.json', 'w', encoding='UTF-8') as json_output:
    json_output.write(json.dumps(travel, indent=4, default=blog_encode))


# Refactored encoder. It leverages the `json` properties in Blog and Post
# objects.
def blog_encoderv2(object):
    if isinstance(object, datetime.datetime):
        return dict(
            __class__   = 'datetime.datetime',
            __args__    = [],
            __kw__      = dict(
                year = object.year,
                month = object.month,
                day = object.day,
                hour = object.hour,
                minute = object.minute,
                second = object.second
            )
        )
    else:
        try:
            encoding = object._json()
        except AttributeError:
            encoding = json.JSONEncoder.default(o)
        return encoding


# Using a custom decoding algorithm
def blog_decode(some_dict):
    if set(some_dict.keys()) == set(["__class__", "__args__", "__kw__"]):
        class_ = eval(some_dict['__class__'])
        return class_(*some_dict['__args__'], **some_dict['__kw__'])
    else:
        return some_dict

with open('/tmp/blogv2.json', 'r', Encoding='UTF-8') as json_input:
    blog = json.loads(json_input.read(), object_hook=blog_decode)
print("Blog Title: {}".format(blog.title))
print("{} Blog Entry 1: {}".format(blog.title, blog.entries[0].get('__kw__').title))
