==============
Roulette Wheel
==============

This project is an implementation of a trivial REST server that provides spins
of a Roulette wheel.

Running
-------
Open up two terminal windows and execute each of the following command::

    $ python server.py
    $ gunicorn --workers=2 app:Wheel3

or::

    $ gunicorn --workers=2 app  # B'se gunicorn looks for the "application" variable, if non is provided, by default.

    $ python client.py

The client issues requests to the server with one-second delays between them.
