import json
from collections.abc import Callable

from wheel import Wheel


class Wheel2(Wheel, Callable):
    def __call__(self, environ, start_response):
        winner = self.spin()
        status = '200 OK'
        headers = [('Content-Type', 'application/json; charset=utf-8')]
        start_response(status, headers)
        return [json.dumps(winner).encode('UTF-8')]
