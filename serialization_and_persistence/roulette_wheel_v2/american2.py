from zero import Zero
from double_zero import DoubleZero
from wheel2 import Wheel2


class American2(Zero, DoubleZero, Wheel2):
    pass
