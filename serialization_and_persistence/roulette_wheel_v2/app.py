from sys import stderr
from collections.abc import Callable
from wsgiref.util import shift_path_info

from american2 import American2
from european2 import European2


class Wheel3(Callable):
    def __init__(self):
        self.am = American2()
        self.eu = European2()

    def __call__(self, environ, start_response):
        request = shift_path_info(environ)
        print("Wheel3", request, file=stderr)
        if request.lower().startswith('eu'):
            response = self.eu(environ, start_response)
        else:
            response = self.am(environ, start_response)

        # print(response)
        return response

application = Wheel3()
