=================================
SQLALchemy Example
=================================

This project showcases how one can build persistence into a Python Application
by leveraging the features of an ORM library. The case study ORM is SQLAlchemy.

Running the app::
    $python app.py
