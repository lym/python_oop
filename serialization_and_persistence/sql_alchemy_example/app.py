import datetime
import sqlalchemy

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from pytz import timezone

from models.base import Base
from models.blog import Blog
from models.tag import Tag
from models.post import posts_tags, Post


def create_database(engine):
    print(Base.metadata)
    Base.metadata.create_all(engine)

def create_session(engine):
    Session = sessionmaker(bind=engine)
    return Session()

if __name__ == '__main__':
    engine = create_engine('sqlite:///./data/blog_dev.db', echo=True)
    create_database(engine)
    session = create_session(engine)

    blog = Blog(title="Travel 2016")
    session.add(blog)

    tags = []
    for phrase in "#RedRanger", "#Whitby42", "#ICW":
        try:
            tags = session.query(Tag).filter(Tag.phrase == phrase).one()
        except sqlalchemy.orm.exc.NoResultFound:
            tag = Tag(phrase=phrase)
            session.add(tag)
        tags.append(tag)

    p2 = Post(
            date=datetime.datetime(2015, 11, 14, 17, 25, tzinfo=timezone('Africa/Kampala')),
            title="Hard Aground",
            rst_text="""Some embarrassing revelation. Including ☹ and ⎕""",
            blog=blog,
            tags=tags
        )

    session.commit()

    # Retrieve posts
    session = create_session(engine)
    for blog in session.query(Blog):
        print("{title}\n{underline}\n".format(**blog.as_dict()))
        for p in blog.entries:
            print(p.as_dict())

    # Querying Post objects, given a tag string
    for post in session.query(Post).join(posts_tags).join(Tag).filter(
            Tag.phrase == "#Whitby42"):
        print(post.blog.title, post.date, post.title, [t.phrase for t in post.tags])
