from sqlalchemy import create_engine

from models.base import Base


def create_database():
    engine = create_engine('sqlite:///./data/blog_dev.db', echo=True)
    print(Base.metadata)
    Base.metadata.create_all(engine)
