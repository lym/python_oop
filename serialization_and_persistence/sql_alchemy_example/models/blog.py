from sqlalchemy import Column, Integer, String
from .base import Base


class Blog(Base):

    __tablename__ = "blogs"
    id = Column(Integer, primary_key=True)
    title = Column(String)

    def as_dict(self):
        return dict(title=self.title, underline='=' * len(self.title),
                    entries=[e.as_dict() for e in self.entries])
