from sqlalchemy import (
        Column, DateTime, Integer, String, UnicodeText, ForeignKey, Table
)
from sqlalchemy.orm import relationship

from .base import Base

posts_tags = Table('posts_tags', Base.metadata,
                         Column('post_id', Integer, ForeignKey('posts.id')),
                         Column('tag_id', Integer, ForeignKey('tags.id')))


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    title = Column(String, index=True)
    date = Column(DateTime, index=True)
    rst_text = Column(UnicodeText)
    blog_id = Column(Integer, ForeignKey('blogs.id'), index=True)
    blog = relationship('Blog', backref='entries')
    tags = relationship('Tag', secondary=posts_tags, backref='posts')

    def as_dict(self):
        return dict(title=self.title, underline='-'*len(self.title),
                    date=self.date, rst_text=self.rst_text,
                    tags=[t.phrase for t in self.tags])
