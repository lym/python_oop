from sqlalchemy import (Column, Integer, String)

from .base import Base


class Tag(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    phrase = Column(String, unique=True)
