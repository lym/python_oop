import sys
import wsgiref.util
import json

from american import American
from european import European

american = American()
european = European()


def wheel(environ, start_response):
    # Extract first portion of environ['PATH_INFO'], what comes after host name
    request = wsgiref.util.shift_path_info(environ)
    print("wheel", request, file=sys.stderr)  # Logging
    if request.lower().startswith('eu'):
        winner = european.spin()
    else:
        winner = american.spin()
    status = '200 OK'
    headers = [('Content-Type', 'application/json; charset=utf-8')]
    start_response(status, headers)
    return [json.dumps(winner).encode('UTF-8')]
