import http.client
import json
import time


def json_get(path='/'):
    rest = http.client.HTTPConnection('localhost', 9000)
    rest.request('GET', path)
    response = rest.getresponse()
    print(response.status, response.reason)
    print(response.getheaders())
    raw = response.read().decode('utf-8')
    if response.status == 200:
        document = json.loads(raw)
        print(document)
    else:
        print(raw)

if __name__ == '__main__':
    print('Connecting to http://localhost:9000')
    for i in range(1, 10):
        json_get()
        time.sleep(1)  # Add one-second delay between requests.
