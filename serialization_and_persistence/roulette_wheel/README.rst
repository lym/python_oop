==============
Roulette Wheel
==============

This project is an implementation of a trivial REST server that provides spins
of a Roulette wheel.

Running
-------
Open up two terminal windows and execute each of the following command::

    $ python server.py

    $ python client.py

The client issues requests to the server with one-second delays between them.
