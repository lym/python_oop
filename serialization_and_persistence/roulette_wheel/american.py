from zero import Zero
from wheel import Wheel
from double_zero import DoubleZero


class American(Zero, DoubleZero, Wheel):
    pass
