from wsgiref.simple_server import make_server

from app import wheel


def roulette_server(count=9):
    httpd = make_server('', 9000, wheel)
    print("Started server at http://localhost:9000")
    if count is None:
        httpd.serve_forever()
    else:
        for c in range(count):
            httpd.handle_request()

if __name__ == '__main__':
    roulette_server()
