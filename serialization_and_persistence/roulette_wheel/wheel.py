import random


class Wheel:
    """Abstract, zero bins omitted."""
    def __init__(self):
        self.rng = random.Random()
        self.bins = [
            {
                str(n): (35, 1),
                self.redblack(n): (1, 1),
                self.hilo(n): (1, 1),
                self.evenodd(n): (1, 1)
            } for n in range(1, 37)
        ]

    @staticmethod
    def redblack(n):
        return "Red" if n in (1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25,
                              27, 30, 32, 34, 36) else "Black"

    @staticmethod
    def hilo(n):
        return "Hi" if n >= 19 else "Lo"

    @staticmethod
    def evenodd(n):
        return "Even" if n % 2 == 0 else "Odd"

    def spin(self):
        return self.rng.choice(self.bins)
