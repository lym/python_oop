CREATE TABLE budgets(
	year INTEGER,
	month INTEGER,
	category TEXT,
	amount DECIMAL
);
