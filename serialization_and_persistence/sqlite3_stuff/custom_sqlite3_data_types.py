""" This module illustrates how we can define custom SQL types in Python and
then use them in our SQL queries issued to sqlite3.

Here we are define a custom "decimal" type. This is because we are dealing
with finance and decimal.Decimal is better suited for these kinds of
calculations then any sqlite native data-type
"""

import decimal
import sqlite3

def adapt_currency(value):
	return str(value)
sqlite3.register_adapter(decimal.Decimal, adapt_currency)

def convert_currency(bytes):
	return decimal.Decimal(bytes.decode())
sqlite3.register_converter("DECIMAL", convert_currency)

database = sqlite3.connect('data/budget_dev.db', detect_types=sqlite3.PARSE_DECLTYPES)
cursor = database.cursor()
with open('sql/create_budget_table.sql', 'r') as script:
    with database:
        cursor.execute(script.read())

with open('sql/budget_record.sql', 'r') as script:
    with database:
        cursor.execute(script.read(), dict(year=2013, month=1, category="fuel",
                                             amount=decimal.Decimal('256.78')))


with open('sql/budget_record.sql', 'r') as script:
    with database:
        cursor.execute(script.read(), dict(year=2013, month=2, category="fuel",
                                             amount=decimal.Decimal('287.65')))

with open('sql/get_budget_record.sql') as script:
    with database:
        for row in cursor.execute(script.read()):
            print(row)

