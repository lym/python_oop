"""Populate a sqlite database, via Python, using SQL, DML, instructions
read from a SQL file.
"""

import sqlite3
app_database    = 'data/blog_dev.db'
sql_script      = 'sql/sql_ddl.sql'
database        = sqlite3.connect(app_database)

with open(sql_script, 'r') as ddl_script:
    with database:
        database.executescript(ddl_script.read())
