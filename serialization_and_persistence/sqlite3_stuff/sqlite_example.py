import sqlite3

conn = sqlite3.connect('data/example.db')

c = conn.cursor()

# Create a table
create_table_query = """CREATE TABLE stocks (date text, trans text, symbol text,
qty real, price real)"""

c.execute(create_table_query)

# Insert a row of data
insert_row_query = """INSERT INTO stocks VALUES ('2006-01-05', 'BUY', 'RHAT', 100,
35.14)"""
c.execute(insert_row_query)

# Save (commit) the changes
conn.commit()

# Closing the connection
conn.close()
