""" This module illustrates how one can define a custom decorator function.
"""

import click
import functools
import logging
import sys

def debug(fn):
	@functools.wraps(fn)
	def logged_function(*args, **kw):
		logging.debug("%s(%r, %r)", fn.__name__, args, kw)
		result = fn(*args, **kw)
		logging.debug("%s = %r", fn.__name__, result)
		return result
	return logged_function

@debug
def ackerman(m,n):
	if m == 0:
		return n + 1
	elif m > 0 and n == 0:
		return ackerman(m - 1, 1)
	elif m > 0 and n > 0:
		return ackerman(m - 1, ackerman(m, n -1))

# Configure logging
# Use root logger
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
click.secho(str(ackerman(2,4)), fg='green')
