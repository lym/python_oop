""" This module illustrates how to use Python's built-in decorators to declare
various kinds of methods i.e, class methods, static methods (functions), and
properties (attribute-like methods)
"""

import math

class Angle(float):
    __slots__ = ("_degrees",)

    @staticmethod
    def from_radians(value):
        return Angle(180 * (value / math.pi))

    def __new__(cls, value):
        self = super().__new__(cls)
        self._degrees = value
        return self

    @property
    def radians(self):
        return math.pi * (self._degrees / 180)

    @property
    def degrees(self):
        return self._degrees

if __name__ == '__main__':
    b = Angle.from_radians(.227)
    print(b.degrees)
