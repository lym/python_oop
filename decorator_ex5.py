""" This module illustrates how one can go about creating and then utilizing
a class (method) decorator.
"""

import click
import logging
import sys

# Define the decorator
# This decorator attaches a `logger` attribute to all classes it decorates
def logged(Klass):
	Klass.logger = logging.getLogger(Klass.__qualname__)
	return Klass

# Apply the decorator
@logged
class SomeClass:
	def __init__(self):
		self.logger.info("New Object of type SomeClass birthed")

	def method(self, *args):
		self.logger.info("method %r", args)

if __name__ == '__main__':

	# Configure logging
	# Use root logger
	logging.basicConfig(stream=sys.stderr, level=logging.INFO)
	baby = SomeClass()
	baby.method("Infant gets new feature :-)")
	#click.secho(baby.method(""), fg='green')
