""" This module illustrates how to leverage Python's built-in decorators to
define comparison operators by not explicitly defining all of their special
mathods but rather just methods for a few. It makes use of the `functools`
module's class decorator.
"""
import click # nonessential; for cute command-line output
import functools

@functools.total_ordering
class Card:
	__slots__ = ("rank", "suit")

	def __new__(cls, rank, suit):
		self = super().__new__(cls)
		self.rank = rank
		self.suit = suit
		return self

	def __eq__(self, other):
		return self.rank == other.rank

	def __lt__(self, other):
		return self.rank < other.rank

if __name__ == '__main__':
	card1 = Card(3, '♠')
	card2 = Card(3, '♥')
	click.secho(str(card1 == card2), fg='green')
	click.secho(str(card1 < card2), fg='red')
	click.secho(str(card1 <= card2), fg='green')
	click.secho(str(card1 >= card2), fg='green', bg='white')
