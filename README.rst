===============================
Object-Oriented Python Examples
===============================

This directory contains code snippets that are birthed as I attempt to get a
deeper understanding of Python's Object-oriented programming model. A lot of
these are inspired by the book *Mastering Object-Oriented Programming* by Steven
F. Lott.

Module Details
==============

This section explains the included modules, why they are relevant, how to
execute them and other useful details.

doctest_example.py
------------------

running::

    $ python doctest_example.py

or::

    $ python doctest_example.py -v


techlogy_spike_modules
----------------------

running::

    $ cd techlogy_spike_modules
    $ python test_runner.py

callable_with_memoization.py
----------------------------

Module that illustrates how one can implement the performace optimization
memoization technique in a callable object.

running::

    $ python callable_with_memoization.py

decorator*.py
-------------

These modules illustrate how we can leverage Python's decorators feature.

running::

    $python decorator*.py

logfile_parser.py
-----------------

This is not a proper OOP module but is a useful little utility. It parses logs
and copies output into a CSV.

running::

    $python logfile_parser [logfile]

robot.py
--------

Module that illustrates how to use Python's "property" decorator.

running::

    $python robot.py

Updating the dependencies file
------------------------------

After specifying the dependency in requirements.ini run::

    $pip-compile requirements.in

Then to install the requirements, from the pip-compile-generated requirements.txt::

    $pip-sync

properties_tester.py
--------------------

Another properties feature illustration

running::

    $python properties_tester.py

context_manager_example.py
--------------------------
Illustrates how to implement context managers.

running::

    $python context_manager_example.py
