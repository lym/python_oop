""" This module illustrates how to leverage Python's Context Managers feature.

The example here alters the functionality of the built-in random module by
implementing a context manager that contains a known seed value hence making
the results from within the context manager deterministic.
"""

import random

class KnownSequence:
	def __init__(self, seed = 0):
		self.seed = 0

	def __enter__(self):
		self.was = random.getstate()
		random.seed (self.seed, version = 1)
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		random.setstate(self.was)

if __name__ == '__main__':
	print(tuple(random.randint(-1, 36) for i in range(5)))
	with KnownSequence():
		print(tuple(random.randint(-1, 36) for i in range(5)))
	print(tuple(random.randint(-1, 36) for i in range(5)))

	with KnownSequence():
		print(tuple(random.randint(-1, 36) for i in range(5)))
	print(tuple(random.randint(-1, 36) for i in range(5)))
