import types
import unittest

class TestAccess(unittest.TestCase):
    def test_should_add_and_get_attribute(self):
        self.object.new_attribute = True
        self.assertTrue(self.object.new_attribute)

    def test_should_fail_on_missing(self):
        self.assertRaises(AttributeError, lambda: self.object.undefined)
