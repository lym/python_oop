import unittest

from abstract_test import TestAccess
from concrete_classes import Test_EmptyClass, Test_Namespace, Test_Object

def suite():
	s = unittest.TestSuite()
	s.addTests(unittest.defaultTestLoader.loadTestsFromTestCase(Test_EmptyClass))
	s.addTests(unittest.defaultTestLoader.loadTestsFromTestCase(Test_Namespace))
	s.addTests(unittest.defaultTestLoader.loadTestsFromTestCase(Test_Object))

	return s

if __name__ == "__main__":
	t = unittest.TextTestRunner()
	t.run(suite())
