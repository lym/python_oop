import types

from abstract_test import TestAccess

class SomeClass:
	pass

class Test_EmptyClass(TestAccess):
	def setUp(self):
		self.object = SomeClass()

class Test_Namespace(TestAccess):
	def setUp(self):
		self.object = types.SimpleNamespace()

class Test_Object(TestAccess):
	def setUp(self):
		self.object = object()
