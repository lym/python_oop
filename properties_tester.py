"""properties_tester.py: Illustrate the use of Python's `@properties` decorator.
`@properties` comes into its own when one wants to achieve
<i>Data Abstraction</i> which is one of the features of the Object-Oriented
Programming approach.

This way of doing things makes it easier for the Producer of a class to update
its internal implementation without breaking the Interface which the clients of
the class already utilize hence being a good sport and upgrading the class
without breaking the clients in the wild.
"""

import click #Optional. Just used here for neat CLI STDOUT output

class A(object):
    def __init__(self):
        self.card = '4♠'    # Private attribute card

    @property
    def card(self):
        return self.__card  # Access private attr as __name

    @card.setter
    def card(self, new_card):
        self.__card = new_card

    @card.deleter
    def card(self):
        self.__card = ''

if __name__ == '__main__':
    a = A()
    click.secho(a.card, fg='green')
    a.card = '8♠'
    click.secho(a.card, fg='red')
    del(a.card)
    click.secho(a.card, fg='red')

    # List all of a's attributes
    for attribute in a.__dir__():
        click.secho(attribute, bold=True)
